export class Produto {

  public nome: string = '';
  public descricao: string = '';
  public categoria: number;
  public preco: number;
  public dataCadastro: Date;
  public tamanho: string = 'p';
  public ativo: boolean = true;
}
