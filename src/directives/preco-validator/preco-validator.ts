import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[precoValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: PrecoValidatorDirective, multi: true }]
})
export class PrecoValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors {
    return precoValidator()(control);
  }
}

export function precoValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    let precoInvalido = false;

    if (control.value) {
      let preco = parseFloat(control.value);

      if (preco <= 0 || preco > 20) {
        precoInvalido = true;
      }
    }

    return precoInvalido ? { 'precoValidator': true } : null;
  };
}
