import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[nomeValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: NomeValidatorDirective, multi: true }]
})
export class NomeValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors {
    return nomeValidator()(control);
  }
}

export function nomeValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    let nome = control.value;

    let nomeInvalido = (nome == 'Hambúrguer' || nome == 'Refrigerante' || nome == 'Batata');

    return nomeInvalido ? { 'nomeValidator': true } : null;
  };
}
