import { NgModule } from '@angular/core';
import { NomeValidatorDirective } from './nome-validator/nome-validator';
import { PrecoValidatorDirective } from './preco-validator/preco-validator';
@NgModule({
	declarations: [
    NomeValidatorDirective,
    PrecoValidatorDirective
  ],
	imports: [],
	exports: [
    NomeValidatorDirective,
    PrecoValidatorDirective
  ]
})
export class DirectivesModule {}
