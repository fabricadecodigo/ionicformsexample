import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Produto } from '../../models/produto';

@IonicPage()
@Component({
  selector: 'page-exemplo-tdf',
  templateUrl: 'exemplo-tdf.html',
})
export class ExemploTdfPage {
  produto: Produto;

  constructor() {
    this.produto = new Produto();
  }

  salvar() {
    console.log(this.produto);
  }
}
