import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExemploTdfPage } from './exemplo-tdf';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    ExemploTdfPage
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(ExemploTdfPage)
  ],
})
export class ExemploTdfPageModule {}
