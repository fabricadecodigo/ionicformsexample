import { DirectivesModule } from './../../directives/directives.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExemploRfPage } from './exemplo-rf';

@NgModule({
  declarations: [
    ExemploRfPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(ExemploRfPage),
  ],
})
export class ExemploRfPageModule {}
