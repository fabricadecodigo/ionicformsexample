import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { nomeValidator } from '../../directives/nome-validator/nome-validator';
import { precoValidator } from '../../directives/preco-validator/preco-validator';

@IonicPage()
@Component({
  selector: 'page-exemplo-rf',
  templateUrl: 'exemplo-rf.html',
})
export class ExemploRfPage {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.createForm();
  }

  private createForm() {
    this.form = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(2), nomeValidator()]],
      descricao: ['', [Validators.required, Validators.minLength(2)]],
      categoria: [''],
      preco: ['', [Validators.required, precoValidator()]],
      dataCadastro: ['', [Validators.required]],
      tamanho: ['p'],
      ativo: [true]
    })
  }

  salvar() {
    if (this.form.valid) {
      console.log(this.form.value);
    }
  }
}
